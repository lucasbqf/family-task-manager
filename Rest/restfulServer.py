from flask_jwt_extended import JWTManager
from flask import Flask,jsonify
from flask_restful import Api
from blocklist import BLOCKLIST
from resources.user import User, UserRegister,UserLogin, UserLogout
from resources.task import Task,Tasks,TaskCreator
from resources.famGroup import FamGroup,FamGroupCreator,FamGroupUsers,FamGroupUser

#definingo configurações do serviço
app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///banco.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://lucasbqf:Pass1234@lucasbqf.mysql.pythonanywhere-services.com/lucasbqf$famTaskManager'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 'osegredo'
app.config['JWT_BLACKLIST_ENABLED'] = True
api = Api(app)
jwt = JWTManager(app)

#criação do banco
@app.before_first_request
def cria_banco():
    banco.create_all()

#verificação de sessão do usuario
@jwt.token_in_blocklist_loader
def verifica_blocklist(self,token):
    return token['jti'] in BLOCKLIST

#revocação do usuario
@jwt.revoked_token_loader
def token_acesso_invalidado(jwt_header, jwt_payload):
    return jsonify({'message': 'voce foi deslogado'}),401



##endpoints:
    #user:
api.add_resource(User, '/user/<int:user_id>')
api.add_resource(UserRegister, '/user/register')
api.add_resource(UserLogin, '/user/login')
api.add_resource(UserLogout, '/user/logout')
    #famGroup:
api.add_resource(FamGroup,'/famgroup/<int:famGroup_id>')
api.add_resource(FamGroupCreator,'/famgroup/create')
api.add_resource(FamGroupUsers ,'/famgroup/<int:famGroup_id>/members')
api.add_resource(FamGroupUser,'/famgroup/<int:famGroup_id>/member')
    #task:
api.add_resource(Tasks,'/famgroup/<int:famGroup_id>/tasks')
api.add_resource(Task,'/famgroup/<int:famGroup_id>/task/<int:task_id>')
api.add_resource(TaskCreator,'/famgroup/<int:famGroup_id>/createtask')



if __name__ == '__main__':
    from sql_alchemy import banco
    banco.init_app(app)
    app.run(debug=True)
