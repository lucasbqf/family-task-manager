from sql_alchemy import banco

class RewardModel(banco.Model):

    __tablename__ = 'rewards'
    reward_id = banco.Column(banco.Integer, primary_key = True)
    user_creator = banco.Column(banco.Integer, banco.ForeignKey('user.user_id') )
    famGroup_id= banco.Column(banco.Integer, banco.ForeignKey('famGroup.famGroup_id') )
    reward_status = banco.Column(banco.Integer)
    titulo = banco.Column(banco.String(64))
    detalhe = banco.Column(banco.String(256))
    #todo datas para encerramento e data da execçao efetiva

    def __init__(self,user_creator,famGroup_id,titulo,detalhe):
        self.user_creator =user_creator
        self.famGroup_id = famGroup_id
        self.titulo = titulo
        self.detalhe = detalhe
        self.reward_status = 0

    def json(self):
        return {'reward_id': reward_id,
        'user_creator':self.user_creator,
        'famGroup_id':self.famGroup_id,
        'titulo':self.titulo,
        'detalhe':self.detalhe,
        'reward_status':self.reward_status
        }
