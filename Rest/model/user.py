from sql_alchemy import banco
from sqlalchemy.orm import relationship

class UserModel(banco.Model):
    __tablename__ = 'users'
    user_id = banco.Column(banco.Integer, primary_key = True)
    login = banco.Column(banco.String(64))
    senha = banco.Column(banco.String(64))
    #nome = banco.Column(banco.String(64))
    #member_of = relationship('FamGroupModel',secondary='famGroup_user_association',backref=banco.backref('fam_mebers'))

    def __init__(self,login,senha):
        self.login = login
        self.senha = senha

    def json(self):
        return {'user_id':self.user_id,
        'login':self.login
        }


    @classmethod
    def find_user(cls,user_id):
        user = cls.query.filter_by(user_id = user_id).first()
        if user:
            return user
        return None

    @classmethod
    def find_by_login(cls,login):
        user = cls.query.filter_by(login = login).first()
        if user:
            return user
        return None

    def save_user(self):
        banco.session.add(self)
        banco.session.commit()

    def update_user(self,senha):
        self.senha = senha

    def delete_user(self):
        banco.session.delete(self)
        banco.session.commit()
