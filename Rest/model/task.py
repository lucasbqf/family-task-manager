from sql_alchemy import banco

class TaskModel(banco.Model):

    __tablename__ = 'tasks'
    task_id = banco.Column(banco.Integer, primary_key = True)
    user_creator = banco.Column(banco.Integer, banco.ForeignKey('users.user_id') )
    famGroup_id= banco.Column(banco.Integer, banco.ForeignKey('famGroups.famGroup_id') )
    task_status = banco.Column(banco.Integer)
    titulo = banco.Column(banco.String(64))
    detalhe = banco.Column(banco.String(256))
    #todo datas para encerramento e data da execçao efetiva

    def __init__(self,user_creator,famGroup_id,titulo,detalhe):
        self.user_creator =user_creator
        self.famGroup_id = famGroup_id
        self.titulo = titulo
        self.detalhe = detalhe
        self.task_status = 0

    def json(self):
        return {'task_id': self.task_id,
        'user_creator':self.user_creator,
        'famGroup_id':self.famGroup_id,
        'titulo':self.titulo,
        'detalhe':self.detalhe,
        'task_status':self.task_status
        }

    @classmethod
    def find_task(cls,task_id):
        task = cls.query.filter_by(task_id = task_id).first()
        if task:
            return task
        return None

    def save_task(self):
        banco.session.add(self)
        banco.session.commit()

    def update_task(self,**kwargs):
        if kwargs['titulo']:
            self.titulo = kwargs['titulo']
        if kwargs['detalhe']:
            self.detalhe = kwargs['detalhe']
        if kwargs['task_status']:
            self.task_status = kwargs['task_status']

        banco.session.commit()

    def delete_task(self):
        banco.session.delete(self)
        banco.session.commit()
