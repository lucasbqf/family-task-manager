from sql_alchemy import banco
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


#famGroup_user_association = banco.Table(
#    'famGroup_user_association',banco.metadata,
#    famGroup_id = Column(ForeignKey('famGroups.famGroup_id'), primary_key=True),
#    #user_level = Column(banco.Integer)
#)

class famGroup_user(banco.Model):
    __tablename__ = 'famGroup_user_association'

    famGroup_id = Column(ForeignKey('famGroups.famGroup_id'), primary_key=True)
    user_id = Column(ForeignKey('users.user_id'), primary_key=True)
    user_level = Column(banco.Integer)

    #def __init__(self,)


class FamGroupModel(banco.Model):
    __tablename__ = 'famGroups'
    famGroup_id = banco.Column(banco.Integer, primary_key = True)
    nome = banco.Column(banco.String(64))
    members = relationship('UserModel',secondary='famGroup_user_association',backref=banco.backref('member_of'))
    user_creator = banco.Column(banco.Integer,nullable=False)



    def __init__(self,user_creator,nome):
        self.nome = nome
        self.user_creator = user_creator

    def json(self):
        return {'famGroup_id':self.famGroup_id,
        'nome':self.nome,
        'user_creator':self.user_creator,
        'members':[member.json() for member in self.members]
        }

    @classmethod
    def find_famGroup(cls,famGroup_id):
        famGroup = cls.query.filter_by(famGroup_id = famGroup_id).first()
        if famGroup:
            return famGroup
        return None

    def save_famGroup(self):
        banco.session.add(self)
        banco.session.commit()

    def update_famGroup(self):
        banco.session.commit()

    def delete_famGroup(self):
        banco.session.delete(self)
        banco.session.commit()
