from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required, get_jwt,get_jwt_identity
from model.famGroup import FamGroupModel
from model.user import UserModel


class FamGroupCreator(Resource):
    atributos = reqparse.RequestParser()
    #atributos.add_argument('user_creator', type=int,required=True, help="campo 'user_creator' nao pode ser deixado em branco")
    atributos.add_argument('nome', type=str,required=True, help= "campo 'nome' nao pode ser deixado em branco")

    @jwt_required()
    def post(self):
        dados = self.atributos.parse_args()
        famGroup = FamGroupModel(get_jwt_identity(),dados['nome'])
        famGroup.members.append(UserModel.find_user(get_jwt_identity()))
        try:
            famGroup.save_famGroup()
            #return{'message':"famGroup '{}' created succesfully!".format(dados['nome'])},201
            return{'message':"famGroup created succesfully!"},201
        except:
            return {'message':'create famGroup db internal error'},500


class FamGroup(Resource):
    def get(self,famGroup_id):
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if famGroup:
            return famGroup.json(),200
        return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

    @jwt_required()
    def delete(self,famGroup_id):
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if famGroup:
            if not famGroup.user_creator == get_jwt_identity():
                return {'message':'you cannot delete this famGroup'},403
            try:
                famGroup.delete_famGroup()
                return{'message': "famGroup '{}' deleted".format(famGroup_id)},200
            except:
                return{'message': 'remove task DB internal error'},500
        return{ 'message':"famGroup with id '{}' dont exist,cannot be deleted".format(famGroup_id)},400


class FamGroupUsers(Resource):
    #retorna todos usuarios do grupo, se tiver acesso
    @jwt_required()
    def get(self,famGroup_id):
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if famGroup:
            members = [member.json() for member in famGroup.members]
            for member in members:
                if member['user_id'] == get_jwt_identity():
                    return {'members':members},200
            return{'message':'you dont have access to this group'},403
        return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

class FamGroupUser(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('login', type=str,required=True, help="field 'login' is required")

    #add user no grupo
    @jwt_required()
    def post(self,famGroup_id):
        dados = self.atributos.parse_args()
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if famGroup:
            members = [member.json() for member in famGroup.members]
            #checa se o usuario tem acesso ao grupo
            for member in members:
                if member['user_id'] == get_jwt_identity():
                    user = UserModel.find_by_login(dados['login'])
                    #checa se o novo usuario existe
                    if not user:
                        return{'message':"user '{}' dont exist".format(dados['login'])}
                    #checa se o login ja esta no grupo
                    for newMember in members:
                        if newMember['user_id'] == user.user_id:
                            return {'message':"user '{}'is already in this group".format(dados['login'])},400
                    famGroup.members.append(user)
                    try:
                        famGroup.update_famGroup()
                        return {'message':"member '{}' added ".format(dados['login'])},200
                    except:
                        return{'message': 'add new member internal DB error'},500

            return{'message':'you dont have access to this group'},403
        return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

        #remove user do grupo
    @jwt_required()
    def delete(self,famGroup_id):
        dados = self.atributos.parse_args()
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if famGroup:
            members = [member.json() for member in famGroup.members]
            #checa se o usuario tem acesso ao grupo
            for member in members:
                if member['user_id'] == get_jwt_identity():
                    toDeleteUser = UserModel.find_by_login(dados['login'])
                    if not toDeleteUser:
                        return{'message':'user dont exist'},400
                    try:
                        famGroup.members.remove(toDeleteUser)
                    except:
                        return{'message':'this user dont exist in this Group'},400
                    try:
                        famGroup.update_famGroup()
                        return {'message':"member '{}' deleted from this group ".format(dados['login'])},200
                    except:
                        return{'message': 'delete member internal DB error'},500
            return{'message':'you dont have access to this group'},403
        return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400
