from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token, jwt_required, get_jwt,get_jwt_identity
from model.task import TaskModel
from model.famGroup import FamGroupModel





class Tasks(Resource):
    @jwt_required()
    def get(self,famGroup_id):
        famGroup = FamGroupModel.find_famGroup(famGroup_id)
        if not famGroup:
            return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

        members = [member.json() for member in famGroup.members]
        for member in members:
            if member['user_id'] == get_jwt_identity():
                return {'tasks':[task.json() for task in TaskModel.query.filter_by(famGroup_id = famGroup_id)]}
            return{'message':'you dont have access to see tasks in this group'},403




class TaskCreator(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('titulo', type=str,required=True)
    atributos.add_argument('detalhe', type=str)

    @jwt_required()
    def post(self,famGroup_id):
        dados = self.atributos.parse_args()
        famGroup= FamGroupModel.find_famGroup(famGroup_id)
        if not famGroup:
            return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

        members = [member.json() for member in famGroup.members]
        for member in members:
            if member['user_id'] == get_jwt_identity():
                task = TaskModel(get_jwt_identity(),famGroup_id,**dados)
                try:
                    task.save_task()
                    return{'message':'task created succesfully!'},201
                except:
                    return {'message':'create task db internal error'},500
        return{'message':'you dont have access to create tasks in this group'},403




class Task(Resource):

    #checa grupo,usuario e task existem, tbm checa se a task é do grupo correto
    def isSomethingWrong(self,user_id,famGroup,task):
        if not famGroup:
            return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400
        if not task:
            return{ 'message':"task with id '{}' dont exist "},400
        if not task.json()['famGroup_id'] ==  famGroup.json()['famGroup_id']:
            return {'message':'this task dont belong to this group'},403
        members = [member.json() for member in famGroup.members]
        for member in members:
            if member['user_id'] == user_id:
                return None
        return{'message':'you dont have access to see tasks in this group'},403




    @jwt_required()
    def get(self,famGroup_id,task_id):
        famGroup = FamGroupModel.find_famGroup(famGroup_id)
        task = TaskModel.find_task(task_id)

        error = self.isSomethingWrong(get_jwt_identity(), famGroup, task)
        if error:
            return error
        return task.json(),200

    @jwt_required()
    def delete(self,famGroup_id,task_id):
        famGroup = FamGroupModel.find_famGroup(famGroup_id)
        task = TaskModel.find_task(task_id)

        error = self.isSomethingWrong(get_jwt_identity(), famGroup, task)
        if error:
            return error
        try:
            task.delete_task()
            return{'message': 'removed'},200
        except:
            return{'message': 'remove task DB internal error'},500

    @jwt_required()
    def put(self,famGroup_id,task_id):
        atributos = reqparse.RequestParser()
        atributos.add_argument('titulo', type=str)
        atributos.add_argument('detalhe', type=str)
        atributos.add_argument('task_status',type = str)
        dados = atributos.parse_args()

        famGroup = FamGroupModel.find_famGroup(famGroup_id)
        task = TaskModel.find_task(task_id)

        error = self.isSomethingWrong(get_jwt_identity(), famGroup, task)
        if error:
            return error
        task.update_task(**dados)
        try:

            return {'message':'task updated'},200
        except:
            return{'message': 'edit task DB internal error'},500
