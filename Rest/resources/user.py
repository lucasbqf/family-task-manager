from flask_restful import Resource, reqparse
from model.user import UserModel
from flask_jwt_extended import create_access_token, jwt_required, get_jwt,get_jwt_identity
from werkzeug.security import safe_str_cmp,generate_password_hash, check_password_hash
from blocklist import BLOCKLIST

atributos = reqparse.RequestParser()
atributos.add_argument('login', type=str,required=True, help="'login' cannot be left blank")
atributos.add_argument('password', type=str,required=True, help="'password' cannot be left blank")

class User(Resource):

    def get(self,user_id):
        user= UserModel.find_user(user_id)
        if user:
            return user.json(),200
        return {'message': 'user not found'},404

    @jwt_required()
    def delete(self,user_id):
        user = UserModel.find_user(user_id)
        if not user_id == get_jwt_identity():
            return {'message':"user '{}', cannot delete user '{}'".format(get_jwt_identity(),user_id)}
        if user:
            try:
                user.delete_user()
                return {'message': 'usuario removido'},200
            except:
                return {'message':'DB delete error'},500
        return {'message': 'usuario nao encontrado'},404

class UserRegister(Resource):
    def post(self):
        dados = atributos.parse_args()
        if UserModel.find_by_login(dados['login']):
            return {'message':"user '{}' already exists".format(dados['login'])}
        hashedPass = generate_password_hash(dados['password'], method='sha256')
        user = UserModel(dados['login'],hashedPass)
        try:
            user.save_user()
            return{'message':"user created successfully!"},201
        except:
            return {'message':'create user db internal error'},500

class UserLogin(Resource):
    @classmethod
    def post(cls):
        dados = atributos.parse_args()

        user = UserModel.find_by_login(dados['login'])
        if user and check_password_hash(user.senha,dados['password']):
            token_acesso = create_access_token(identity = user.user_id)
            return {'access_token': token_acesso},200
        return {'message':'usuario ou senha incorreto'},401

####nao funciona!, nao esta terminado.
class MyGroups(Resource):
    @jwt_required()
    def get(self):
        #famGroups= FamGroupModel.find_famGroup(famGroup_id)
        myfamGoups = UserModel.find_user(get_jwt_identity())
        if myfamGoups:
            return myfamGoups.json(),200
        return{ 'message':"famGroup with id '{}' dont exist ".format(famGroup_id)},400

class UserLogout(Resource):
    @jwt_required()
    def post(self):
        jwt_id = get_jwt()['jti']
        BLOCKLIST.add(jwt_id)
        return {'message': 'Logout realizado com sucesso'}
